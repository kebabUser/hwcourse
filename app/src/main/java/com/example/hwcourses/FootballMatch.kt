package com.example.hwcourses

//Создадим класс FootballMatch
data class FootballMatch(var pointsOfFirstTeam: Int = 0, var pointsOfSecondTeam: Int = 0){
    //Функция для задания очков команд
    fun setPoints(firstPoints: Int, secondPoints: Int) {
        pointsOfFirstTeam = firstPoints;
        pointsOfSecondTeam = secondPoints;
    }
}