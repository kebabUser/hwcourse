package com.example.hwcourses

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlin.math.abs

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        println()
        println("Start")
        //Создадим экземпляр класса FootballMatch и выведем его
        var match = FootballMatch()
        println(match)
        println("*****")

        //Изменим зададим очки команд и выведем результат
        match.setPoints(3, 2)
        println(match)
        println("*****")

        //Создадим массив из 10 футбольных матчей, зададим значения и выведем результат
        val arrayOfMatches = ArrayList<FootballMatch>()
        for (i in 0..9) {
            arrayOfMatches.add(FootballMatch((0..5).random(), (0..5).random()))
        }
        arrayOfMatches.forEach {
            println(it)
        }
        println("*****")

        //Удалим все матчи с одинаковыми очками команд и выведем результат
        val clearArrayOfMatches = ArrayList<FootballMatch>()
        arrayOfMatches.forEach {
            if (it.pointsOfFirstTeam != it.pointsOfSecondTeam) {
                clearArrayOfMatches.add(it)
            }
        }
        clearArrayOfMatches.forEach {
            println(it)
        }
        println("*****")

        //Создадим множество(Set) с максимальным разрывом в очках и выведем результат
        var max = 0
        clearArrayOfMatches.forEach {
            if (abs(it.pointsOfFirstTeam - it.pointsOfSecondTeam) > max) {
                max = abs(it.pointsOfFirstTeam - it.pointsOfSecondTeam)
            }
        }
        val setMatches = mutableSetOf<FootballMatch>()
        clearArrayOfMatches.forEach {
            if (abs(it.pointsOfFirstTeam - it.pointsOfSecondTeam) == max) {
                setMatches.add(it)
            }
        }
        setMatches.forEach{
            println(it)
        }

        println("The end.")
    }
}